import TextField from '@material-ui/core/TextField';
import * as React from 'react';

interface Props {
  label: string;
  type: string;
  value: string;
  onChange(event: React.ChangeEvent): void;
}

// tslint:disable-next-line: no-default-export
export default class DialogInput extends React.PureComponent<Props> {
  render() {
    return (
      <TextField
        margin="none"
        fullWidth={true}
        required={true}
        label={this.props.label}
        type={this.props.type}
        value={this.props.value}
        onChange={this.props.onChange}
      />
    );
  }
}

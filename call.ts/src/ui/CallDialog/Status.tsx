import Snackbar from '@material-ui/core/Snackbar';
import * as React from 'react';
import { connect } from 'react-redux';
import {
  Props,
  mapStateToProps,
  mapDispatchToProps,
} from 'store/ui/CallDialog/Status';

export class Status extends React.PureComponent<Props> {
  render() {
    return (
      <Snackbar
        anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
        open={this.props.open}
        autoHideDuration={6000}
        onClose={this.props.close}
        ContentProps={{
          'aria-describedby': 'call-status-message',
        }}
        message={<span id="call-status-message">{this.props.message}</span>}
      />
    );
  }
}

// tslint:disable-next-line: no-default-export
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Status);

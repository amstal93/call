import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import LinearProgress from '@material-ui/core/LinearProgress';
import * as React from 'react';
import { connect } from 'react-redux';
import Form from 'ui/CallDialog/Form';
import Status from 'ui/CallDialog/Status';
import {
  Props,
  mapStateToProps,
  mapDispatchToProps,
} from 'store/ui/CallDialog';

export class CallDialog extends React.PureComponent<Props> {
  render() {
    return (
      <React.Fragment>
        <Dialog
          open={this.props.open}
          onClose={this.props.close}
          aria-labelledby="call-dialog-title"
          fullScreen={this.props.isMobile}
        >
          <DialogTitle id="call-dialog-title">Send Your Number</DialogTitle>
          <DialogContent>
            <DialogContentText>
              I <b>ignore unknown</b> numbers.
              <br />
              <b>Let me know</b> it is You!
              <br />
              <br />
              <i>
                P.S. Maybe I am just <b>busy</b>
              </i>
            </DialogContentText>
            <Form />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.props.close} color="secondary">
              Cancel
            </Button>
            <Button
              onClick={this.props.sendNotification}
              color="primary"
              disabled={this.props.isSending}
            >
              Send
            </Button>
          </DialogActions>
          {this.props.open && this.props.isSending && <LinearProgress />}
        </Dialog>
        <Status />
      </React.Fragment>
    );
  }
}

// tslint:disable-next-line: no-default-export
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CallDialog);

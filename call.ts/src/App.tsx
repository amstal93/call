import loadable from '@loadable/component';
import CssBaseline from '@material-ui/core/CssBaseline';
import { MuiThemeProvider } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import * as React from 'react';
import { Provider } from 'react-redux';
import 'ui/css/App.css';
import { CallButton, ScreenInfo } from 'ui';
import { APP_THEME } from 'Config';
import { APP_STORE } from 'store';

// tslint:disable: variable-name
const CallDialog = loadable(() =>
  import(/* webpackChunkName: "CallDialog" */ 'ui/CallDialog')
);
const Footer = loadable(() =>
  import(/* webpackChunkName: "Footer" */ 'ui/Footer')
);

export class App extends React.PureComponent {
  render() {
    return (
      <MuiThemeProvider theme={APP_THEME}>
        <CssBaseline />
        <Provider store={APP_STORE}>
          <ScreenInfo />
          <div className="centered_container">
            <Typography variant="h4" align="center">
              PLEASE{' '}
              <b style={{ color: APP_THEME.palette.primary.main }}>ANSWER</b>
            </Typography>
            <CallButton />
          </div>
          <CallDialog />
        </Provider>
        <Footer />
      </MuiThemeProvider>
    );
  }
}

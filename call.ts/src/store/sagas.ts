import { takeLatest } from 'redux-saga/effects';
import { Actions } from 'store/actions';
import { sendCallNotification } from 'store/reducers/CallDialog';

export function* sendCallNotificationSaga() {
  yield takeLatest(Actions.CALL_DIALOG_SEND, sendCallNotification);
}

import { Actions, CallStatusAction } from 'store/actions';

export function callStatusOpen(message: string): CallStatusAction {
  return { type: Actions.CALL_STATUS_OPEN, message };
}

export const callStatusClose: CallStatusAction = {
  type: Actions.CALL_STATUS_CLOSE,
};

import { TextFieldProps } from '@material-ui/core/TextField';
import { Actions, CallFormAction } from 'store/actions';

export function callFormNameChange(event: React.ChangeEvent): CallFormAction {
  return {
    type: Actions.CALL_FORM_NAME_CHANGE,
    value: (event.target as TextFieldProps).value as string,
  };
}

export function callFormPhoneChange(event: React.ChangeEvent): CallFormAction {
  return {
    type: Actions.CALL_FORM_PHONE_CHANGE,
    value: (event.target as TextFieldProps).value as string,
  };
}

import { Actions, CallDialogAction } from 'store/actions';

export const callDialogOpen: CallDialogAction = {
  type: Actions.CALL_DIALOG_OPEN,
};

export const callDialogClose: CallDialogAction = {
  type: Actions.CALL_DIALOG_CLOSE,
};

export const callDialogSend: CallDialogAction = {
  type: Actions.CALL_DIALOG_SEND,
};

export function callDialogSendSuccess(status: string): CallDialogAction {
  return { type: Actions.CALL_DIALOG_SEND_OK, status };
}

export function callDialogSendFail(message: string): CallDialogAction {
  return { type: Actions.CALL_DIALOG_SEND_FAIL, message };
}

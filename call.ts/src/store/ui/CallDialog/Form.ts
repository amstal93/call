import { Dispatch } from 'redux';
import {
  callFormNameChange,
  callFormPhoneChange,
} from 'store/actions/CallDialog/Form';
import { AppState } from 'store/State';

interface State {
  name: string;
  phone: string;
  error: string;
}

interface Actions {
  onNameChange(event: React.ChangeEvent): void;
  onPhoneChange(event: React.ChangeEvent): void;
}

export interface Props extends State, Actions {}

export function mapStateToProps(state: AppState): State {
  return {
    name: state.callFormName,
    phone: state.callFormPhone,
    error: state.callFormError,
  };
}

export function mapDispatchToProps(dispatch: Dispatch): Actions {
  return {
    onNameChange: (event: React.ChangeEvent) =>
      dispatch(callFormNameChange(event)),
    onPhoneChange: (event: React.ChangeEvent) =>
      dispatch(callFormPhoneChange(event)),
  };
}

import { Dispatch } from 'redux';
import { callDialogClose, callDialogSend } from 'store/actions/CallDialog';
import { AppState } from 'store/State';

interface State {
  open: boolean;
  isSending: boolean;
  isMobile: boolean;
}

interface Actions {
  close(): void;
  sendNotification(): void;
}

export interface Props extends State, Actions {}

export function mapStateToProps(state: AppState): State {
  return {
    open: state.callDialogOpen,
    isSending: state.callDialogIsSending,
    isMobile: state.screen.isMobile,
  };
}

export function mapDispatchToProps(dispatch: Dispatch): Actions {
  return {
    close: () => dispatch(callDialogClose),
    sendNotification: () => dispatch(callDialogSend),
  };
}

import { Dispatch } from 'redux';
import { screenUpdate } from 'store/actions/ScreenInfo';
import { ScreenData } from 'DataTypes';

export interface Props {
  update(screen: ScreenData): void;
}

export function mapDispatchToProps(dispatch: Dispatch): Props {
  return {
    update: (screen: ScreenData) => {
      dispatch(screenUpdate(screen));
    },
  };
}

import { ScreenData } from 'DataTypes';
import { getScreenData } from './utils';

interface ScreenState {
  screen: ScreenData;
}

interface CallDialogState {
  callDialogOpen: boolean;
  callDialogIsSending: boolean;
}

interface CallFormState {
  callFormName: string;
  callFormPhone: string;
  callFormError: string;
}

interface CallStatusState {
  callStatusOpen: boolean;
  callStatusMessage: string;
}

export interface AppState
  extends ScreenState,
    CallDialogState,
    CallFormState,
    CallStatusState {}

export const INITIAL_STATE: AppState = {
  screen: getScreenData(),
  callDialogOpen: false,
  callDialogIsSending: false,
  callFormName: '',
  callFormPhone: '',
  callFormError: '',
  callStatusOpen: false,
  callStatusMessage: '',
};

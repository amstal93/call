import { INITIAL_STATE, AppState } from 'store/State';
import { update } from 'store/utils';
import { Actions, CallFormAction, CallDialogAction } from 'store/actions';

export function callFormReducer(
  state = INITIAL_STATE,
  action: CallFormAction | CallDialogAction
): AppState {
  switch (action.type) {
    case Actions.CALL_FORM_NAME_CHANGE:
      return update(state, {
        callFormName: action.value,
        callFormError: '',
      });
    case Actions.CALL_FORM_PHONE_CHANGE:
      return update(state, {
        callFormPhone: action.value,
        callFormError: '',
      });
    case Actions.CALL_DIALOG_OPEN:
      return update(state, {
        callFormName: '',
        callFormPhone: '',
        callFormError: '',
      });
    case Actions.CALL_DIALOG_SEND_FAIL:
      return update(state, { callFormError: action.message });
    default:
      return state;
  }
}

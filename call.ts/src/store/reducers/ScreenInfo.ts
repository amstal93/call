import { AppState, INITIAL_STATE } from 'store/State';
import { update } from 'store/utils';
import { Actions, ScreenAction } from 'store/actions';

export function screenInfoReducer(
  state = INITIAL_STATE,
  action: ScreenAction
): AppState {
  switch (action.type) {
    case Actions.SCREEN_UPDATE:
      return update(state, { screen: action.screen });
    default:
      return state;
  }
}

Micro-service for those, who does not answer to unknown numbers and want people to notify them prior to calling.

Mine is hosted on https://call.neigor.me

## You can host it on your server!

Manual can be found in `How Do I Run It` section of this blog post: https://blog.neigor.me/call

Just clone this repository and do these steps:
* Do `npm install` in frontend folder
* Copy .env to .env.production.local and change BOT_TOKEN,CHAT_ID, TIMEZONE
* Run `./scripts/production PORT_NUMBER` (0 as port will create Unix Domain socket `call-nginx.sock` in volume `nginx`)

(Note: you might need `sudo` to run commands above since they use Docker and npm)

`TIMEZONE` is optional. If it is not provided - `"UTC"` will be used!

I use [Docker Webserver](https://gitlab.com/harry.sky.vortex/docker-webserver)
as `TLS termination reverse-proxy` in front of this micro-service,
since this nginx.conf is very minimal and for HTTP only.
I also set port to 0 so `Docker Webserver` and `Call` communicate via
Unix Domain socket (which is a lot faster than TCP/IP in Docker)
